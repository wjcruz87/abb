from app import app, db
from flask import request
from model.day import Day
from model.ip_view import IpView
from sqlalchemy.sql.functions import count
import datetime
from flask import jsonify

def get_current_day():
    today = datetime.date.today()
    return today.strftime("%Y-%m-%d")

@app.route("/version", methods=["GET"])
def on_view():
    try:
        ###App version
        ver = {
            "app_version":0.1,
            "company":"ABB"
        }
        return jsonify(ver), 200  ## json conversion and view
    except Exception as e:
        return {"error":True, "message": str(e)}, 500

@app.route("/", methods=["GET"])
def get_analytics():
    try:
        ### count & DB
        day_id = get_current_day() # get our day_id, which is the date string in the format "yyyy-mm-dd"
        client_ip = request.remote_addr # get the ip address of where the client request came from

        query = Day.query.filter_by(id=day_id) # try to get the row associated to the current day
        if query.count() > 0: 
            # the current day is already in table, simply increment its views
            current_day = query.first()
            current_day.views += 1
        else:
            # the current day does not exist, its the first view for the day.
            current_day = Day(day_id, 1, 0)
            db.session.add(current_day) # insert a new day into the day table
        
        query = IpView.query.filter_by(ip=client_ip,date_id=day_id)
        if query.count() == 0: # check if its the first time a viewer from this ip address is viewing the website
            ip_view = IpView(client_ip, day_id)
            db.session.add(ip_view) # insert into the ip_view table
        
        db.session.commit() # commit all the changes to the database
        ### results
        results = db.session.query(Day.id, Day.views, Day.reads, count(Day.id)).\
                  select_from(Day).join(IpView).\
                  group_by(Day.id).order_by(Day.id).all()

        days = []

        for result in results:
            days.append({
                "day_id": str(result[0]),
                "views": result[1],
                "viewers": result[3]
            })
            

        return {"days": days}, 200
    except Exception as e:
        return {"error": True, "message": str(e)}, 500

if __name__ == "__main__":
   app.run(debug=True, host='0.0.0.0')
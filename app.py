#Flask application
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
#from config import DATABASE_URI

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["DATABASE_URI"]#pass from env
db = SQLAlchemy(app)
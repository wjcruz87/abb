#outputs

output "load_balancer_ip" {
  value = aws_lb.default.dns_name
}

output "psql_password" {
  value = random_string.psql_password.result
}

output "db_instance_address" {
  value = try(aws_db_instance.this[0].address, "")
}
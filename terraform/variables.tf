#Used to desired_count on Service to number of instances
variable "app_count" {
  type = number
  default = 1
}
variable "db_instance_type" {}
variable "psql_username" {
  type = string
  default = "postgres"
}
variable "profile" {}
terraform {
    backend "s3" {
        bucket = "abb-terraform-tfstate"
        encrypt = true
        key = "abb-app-instance/state"
        region = "us-west-1"
        profile = var.profile
    }
}
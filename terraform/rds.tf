#************************************************************************************
#CREATE SG and PSW
#************************************************************************************
resource "random_string" "psql_password" {
  length  = 32
  upper   = true
  number  = true
  special = false
}

resource "aws_security_group" "psql_sg" {
  vpc_id      = aws_vpc.public.id
  name        = "sg-psql-abb"
  description = "Allow all inbound for Postgres"
ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private.id]
  }
depends_on = [
    aws_vpc.default,
    aws_subnet.private,
]
}

#************************************************************************************
#CREATE RDS PSQL
#************************************************************************************
resource "aws_db_instance" "psql_db" {
  identifier             = "psql-abb"
  name                   = "psql-abb"
  instance_class         = var.db_instance_type
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "12.5"
  skip_final_snapshot    = true
  publicly_accessible    = true
  db_subnet_group_name   = aws_db_subnet_group.private.id
  vpc_security_group_ids = [aws_security_group.sg_task.id]
  username               = var.psql_username
  password               = random_string.psql_password.result
depends_on = [
    aws_security_group.psql_sg,
]
}
#ECS task definition
#awsvpc to elastic network interface and priv IP
resource "aws_ecs_task_definition" "abb_app" {
  family                   = "ecs_task_abb_app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048

  container_definitions = <<DEFINITION
[
  {
    "image": "abb.dkr.ecr.us-east-1.amazonaws.com/abb-visitors:latest",
    "cpu": 1024,
    "memory": 2048,
    "name": "abb-visitors-app",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 5000,
        "hostPort": 5000
      }
    ]
  }
]
DEFINITION
}   
###########################################################################
#Security Group for this

resource "aws_security_group" "sg_task" {
  name        = "sg-task-abb-app"
  vpc_id      = aws_vpc.default.id

  ingress {
    protocol        = "tcp"
    from_port       = 5000
    to_port         = 5000
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#############################################################################
#ECS Cluster & Service
resource "aws_ecs_cluster" "main" {
  name = "abb-cluster"
}

resource "aws_ecs_service" "abb_app" {
  name            = "svc-abb-app"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.abb_app.arn
  #The app_count VARIABLE:
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.sg_task.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.abb_app.id
    container_name   = "abb-visitors-app"
    container_port   = 5000
  }

  depends_on = [aws_lb_listener.abb_app]
}
ABB Site Counter App.

Local Docker mode:

    steps:

        - docker volume create psql-volume-abb
  
        - docker run -d --name=postgres14 -p 5432:5432 -v psql-volume-abb:/var/lib/postgresql/data --net=bridge postgres
  
        ## We generate volume for database persistence and net=bridge for networking connection

        - docker inspect postgres14
        
        - generate file "env.e" with psql string connection like: DATABASE_URI=postgresql://postgres:password@POSTGRESQL_IP:5432/analytics
        
        - docker build -t abb_visit:latest .
        
        - docker run --env-file=env.e --net=bridge -d -p 5000:5000 abb_visit:latest
        
        ## same net=bridge


Terraform:

    Ubication terraform folder.

    vars:

        - db_instance_type = instance type of database

        - psql_username = default postgres

        - profile = AWS profile
    

    This terraform create ECS cluster, ECR repository and RDS Postgresql database to support the application.

    The image is builded from any CI and uploaded to ECR.


    tfstate it's saved on S3 bucket for sharing and persistence.


Gitlab-CI_:

    - Create user with policys in AWS.

    - Create repository variable AWS_ACCESS_KEY_ID (protected)

    - Create repository variable AWS_SECRET_ACCESS_KEY (protected)

    - Create repository variable URL_REGISTRY

    - Create repository variable AWS_REGION

    - Create repository variable DATABASE_URI (protected)


How to monitoring this:

    To monitoring this I would use a Datadog Agent deploying it in ECS Cluster.

    This can gather logs, metrics and request from Docker's there.

    After that I would create a Dashboard with the significatly metrics.

    For the database I can configure Datadog with CloudWatch to recive metrics and I would configure the preset dashboard with all information.

    With all of this I would create email notifications for the critical cases.


DOCUMENTATION:

    abb.drawio.pdf It's a diagram of Infrastructure

    About the technologies I'm using Docker, Dockercompose, git (gitlab), Terraform, Datadog, AWS : ECS, RDS, ECR, Load Balancer, Security Groups, Subnets, Gateway.


    To start from scratch:

        -   Download repository

        -   If you like to run this locally got to step: "Local Docker mode"

        -   If you need to put this in the cloud. Run terraform only ecr.tf file to create image repository.

        -   Run gitlabCI to create the image.

        -   Run terraform to create infrastructure, setting variables. This with the image start the application.
    

    To scale app:

        To increade or decrease the application We can configure the service autoscaling of AWS ECS Fargate. This run with Cloudwatch to increase and decrease the 
        autoscaling based on scaling policy.
        
        For the database RDS We can configure the RDS Storage Auto Scaling 